const initialState = {}; // Якщо в умові не вказано конкретно

const yourReducer = (state = initialState, action) => {
    switch (action.type) {
        default:
            return state;
    }
};

export default yourReducer;